import { Module } from '@nestjs/common';
import { AppController } from './controller/app.controller';
import { AppService } from './service/app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { userSchema } from './schema/user.schema';
import { UserController } from './controller/user.controller';
import { JwtAuthenService, JwtStrategy } from './service/jwt-authen.service';
import { accessTokenSchema } from './schema/access-token.schema';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/member_db'),
    MongooseModule.forFeature([
      // { name: 'Cat', schema: CatSchema },
      { name: 'User', schema: userSchema },
      { name: 'AccessToken', schema: accessTokenSchema }
    ])
  ],
  controllers: [
    AppController,
    UserController
  ],
  providers: [
    AppService,
    JwtAuthenService,
    JwtStrategy
  ],
})
export class AppModule {}
