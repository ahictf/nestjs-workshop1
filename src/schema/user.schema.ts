import {Schema} from 'mongoose';

export const userSchema = new Schema({
    firstname: String,
    lastname: String,
    email: String,
    password: String,
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    },
}, {
    toObject: {
        virtuals: true
    }
});