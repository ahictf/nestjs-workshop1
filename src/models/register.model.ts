import { IsEmail, IsNotEmpty, Length, Max, MaxLength, Min, MinLength } from "class-validator";
import { interface_Register } from "src/interfaces/app.interface";

export class RegisterModel implements interface_Register {
    @IsNotEmpty({message:'กรุณากรอกชื่อ'})
    @MaxLength(50,{message:'ชื่อยาวเกินไป'})
    firstname: string;

    @IsNotEmpty({message:'กรุณากรอกนามสกุล'})
    @MaxLength(50,{message:'นามสกุลยาวเกินไป'})
    lastname: string;

    @IsNotEmpty({message:'กรุณากรอกข้อมูลอีเมล'})
    @IsEmail({},{message:'รูปแบบอีเมลไม่ถูกต้อง'})
    @MaxLength(50,{message:'อีเมลยาวเกินไป'})
    email: string;
    
    @IsNotEmpty({message:'กรุณากรอกรหัสผ่าน'})
    @MinLength(8,{message: 'รหัสผ่านต้อง 8 ตัวอักษรขึ้นไป'})
    @MaxLength(200,{message:'รหัสผ่านต้องไม่เกิน 200 ตัวอักษร'})
    password: string;

}

