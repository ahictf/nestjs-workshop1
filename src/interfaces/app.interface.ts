export interface interface_User {
    _id?: any;
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    created?: Date;
    updated?: Date;
}

export interface interface_Register {
    firstname: string;
    lastname: string;
    email: string;
    password: string;
}

export interface interface_Login {
    email: string;
    password: string;
}