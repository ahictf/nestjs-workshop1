import { Document } from "mongoose";

export interface interface_AccessTokenDocument extends Document {
    userID : any;
    accessToken : string;
    exprise : Date;
    created : Date;
}