import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { interface_Login, interface_Register, interface_User } from 'src/interfaces/app.interface';
import { IMemberDocument } from 'src/interfaces/user.interface';
import * as bcrypt from 'bcrypt';
import { JwtAuthenService } from './jwt-authen.service';

@Injectable()
export class AppService {

  constructor(
    private authenService: JwtAuthenService,
    @InjectModel('User') private UserCollection: Model<IMemberDocument>) {
    
  }

  // Register
  async onRegister(body: interface_Register){
    const count = await this.UserCollection.count({email: body.email});
    if(count > 0){
      throw new BadRequestException('มีอีเมลในระบบแล้ว')
    }
    const model: interface_User = body;
    const salt = await bcrypt.genSalt(8);
    const passwordHash = await bcrypt.hash(model.password,salt);
    model.password = passwordHash
    return await this.UserCollection.create(model);
  }

  // Login
  async onLogin(body: interface_Login){
    const user = await this.UserCollection.findOne({ email: body.email});
    if(!user){
      throw new BadRequestException('อีเมลหรือรหัสผ่านไม่ถูกต้อง')
    }
    const passwordCheck = await bcrypt.compare(body.password,user.password)
    if(!passwordCheck){
      throw new BadRequestException('อีเมลหรือรหัสผ่านไม่ถูกต้อง')
    }
    return { 
      accessToken: await this.authenService.generateAccessToken(user)
    }
  }
  
}
