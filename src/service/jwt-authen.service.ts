import { Injectable, UnauthorizedException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { interface_AccessTokenDocument } from "src/interfaces/access-token.interface";
import { IMemberDocument } from "src/interfaces/user.interface";
import * as jwt from 'jsonwebtoken'
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from 'passport-jwt';
// import { jwtConstants } from './constants';

@Injectable()
export class JwtAuthenService {
    constructor(@InjectModel('User') private UserCollection: Model<IMemberDocument>) { }
    // constructor(@InjectModel('AccessToken') private AccessTokenColloection: Model<interface_AccessTokenDocument>) { }
    
    static secretKey:string = 'WHyrDzO3^@^h#036td&XRhT4PF8U%A!&^VAzKE1Z'
    
    async generateAccessToken(user: IMemberDocument) {
        const payload = { email: user.email}
        return jwt.sign(payload, JwtAuthenService.secretKey, { expiresIn: 60*60 })
    }

    async validateUser({ email }): Promise<IMemberDocument> {
        
        try {
            return this.UserCollection.findOne({ email });
        }
        catch (e) { }
        return null;
    }
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: JwtAuthenService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: JwtAuthenService.secretKey,
    });
  }

  async validate(payload: {email:string}, done: Function) {
    const user = await this.authService.validateUser(payload)
        if (!user) {
            return done(new UnauthorizedException('Unauthorized please login!'), false);
        }
        done(null, user);
  }
}