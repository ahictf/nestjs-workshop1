import { Body, Controller, Get, Post, Req, UseGuards } from "@nestjs/common";
import { IMemberDocument } from "src/interfaces/user.interface";
import { LoginModel } from "src/models/login.model";
import { RegisterModel } from "src/models/register.model";
import { ValidationPipe } from "src/pipes/validation.pipe";
import { AppService } from "src/service/app.service";
import { Request } from 'express';
import { AuthGuard } from "@nestjs/passport";

@Controller('api/users')
export class UserController {
    constructor(private service: AppService){}

    @Post('register') // Register
    async register(@Body(new ValidationPipe()) body: RegisterModel){
        const memberItem = await this.service.onRegister(body)
        return memberItem
    }

    @Post('login') // Login
    login(@Body(new ValidationPipe()) body: LoginModel){
        return this.service.onLogin(body)
    }

    @Get('profile/me')
    @UseGuards(AuthGuard('jwt'))
    getProfile(@Req() req: Request){
        const userLogin: IMemberDocument = req.user as any;
        return userLogin
    }

}